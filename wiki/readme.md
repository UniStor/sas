[Sch:](https://www.google.com/search?q=camcontrol+format)

## [How to Use Drives with Abnormal Sector Sizes](https://www.truenas.com/community/threads/how-to-use-drives-with-abnormal-sector-sizes.51245/) - truenas.com
[Ark](http://web.archive.org/web/20201103114441/https://www.truenas.com/community/threads/how-to-use-drives-with-abnormal-sector-sizes.51245/)

quote: '''BTW, it's a good idea to run "camcontrol format device -r" after starting the format.

The format command above will not show progress and will time out after 3 hours even if the format is still running. This is OK, because the disk controller does not need OS interaction, but you'll want to know the format is finished before removing the disk. The following will show progress:

root@compute-002:/home/bacon # camcontrol format da3 -r
Formatting: 37.61 % (24649/65536) done'''
—[#post-576940](https://www.truenas.com/community/threads/how-to-use-drives-with-abnormal-sector-sizes.51245/#post-576940)

## [Repurposing netapp disk trays with FreeBSD and ZFS](http://www.sysop.ca/?p=208)
[Ark](http://web.archive.org/web/20201103113724/http://www.sysop.ca/?p=208)

## [Reformatting NetApp Disks from 520 sector size to 512 sector size using FreeNAS (FreeBSD)](http://www.davidrusseltrask.com/blog/2019/6/16/reformatting-netapp-disks-from-520-sector-size-to-512-sector-size-using-freenas-freebsd)
[Ark](http://web.archive.org/web/20201103114107/http://www.davidrusseltrask.com/blog/2019/6/16/reformatting-netapp-disks-from-520-sector-size-to-512-sector-size-using-freenas-freebsd)

quote: '''What’s the worst that can happen? So I look for the cmd flag in the man page to see what it does.

camcontrol cmd [device id] [generic args] ⟨-a cmd [args]⟩ ⟨-c cmd [args]⟩ [-d] [-f] [-i len fmt] [-o len fmt [args]] [-r fmt]

Great not helpful. But “-v” is for verbose output of failed SCSI commands after more googling and man page reading I learned that “-c” sends SCSI CDB (command descriptor block) which according to wikipedia. “SCSI commands are sent in a command descriptor block (CDB), which consists of a one byte operation code (opcode) followed by five or more bytes containing command-specific parameters. Upon receiving and processing the CDB the device will return a status code byte and other information.'''