# error: MODE SELECT command: Illegal request sense key, apart from Invalid opcode
https://www.truenas.com/community/threads/formatting-netapp-seagate-constellation-es-3-to-512-byte-blocks.55159/

sch: https://www.google.com/search?q=MODE+SELECT+command%3A+Illegal+request+sense+key%2C+apart+from+Invalid+opcode

# Try
I used the instructions from the article http://www.sysop.ca/?p=208 and was able to reformat some NetApp drives and use them with FreeNAS. I will say that the camcontrol command that does the reformatting takes a REALLY LONG TIME to complete.
— http://www.sysop.ca/?p=208